package com.company;

import com.company.core.ProgramEngineImpl;
import com.company.core.contracts.ProgramEngine;

public class StartEngine {

    public static void main(String[] args) {
        ProgramEngine engine = new ProgramEngineImpl();
        engine.start();
    }
}

