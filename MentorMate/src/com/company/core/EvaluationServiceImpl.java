package com.company.core;

import com.company.core.contracts.EvaluationService;
import com.company.models.contracts.Employee;
import com.company.models.contracts.Report;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;

public class EvaluationServiceImpl implements EvaluationService {
    private List<Employee> employees;
    private Report report;

    public EvaluationServiceImpl(List<Employee> employees, Report report) {
        this.employees = employees;
        setReport(report);
    }

    @Override
    public List<Employee> getEmployees() {
        return new ArrayList<>(employees);
    }

    @Override
    public Report getReport() {
        return report;
    }

    @Override
    public void createCSVFile() throws IOException {
        Map<Employee, Double> topEmployees = evaluateEmployees();

        try (FileWriter writer = new FileWriter("Result.csv")) {
            StringBuilder sb = new StringBuilder();
            sb.append("Name");
            sb.append(" , ");
            sb.append("Score");
            sb.append("\r\n");

            for (Map.Entry<Employee, Double> employeeEntry : topEmployees.entrySet()) {
                sb.append(employeeEntry.getKey().getName());
                sb.append(", ");
                sb.append(employeeEntry.getValue());
                sb.append("\r\n");
            }

            writer.write(sb.toString());
            writer.close();
            System.out.println("===DONE!===");
        } catch (FileNotFoundException e) {
            System.out.println(e.getMessage());
        }
    }

    private void setEmployees(List<Employee> employees) {
        this.employees = employees;
    }

    private void setReport(Report report) {
        if (report.getTopPerformersThreshold() > 100) {
            throw new IllegalArgumentException("Top performers threshold percents must be between 1% and 100%");
        }
        this.report = report;
    }

    private Map<Employee, Double> evaluateEmployees() {
        List<Employee> filteredEmployees = validateSalesPeriod(getEmployees());

        Map<Employee, Double> sortedEmployeesByScore =
                sortEmployeesByScore(calculateEmployeeScores(filteredEmployees));

        return findTopEmployees(sortedEmployeesByScore);
    }


    private List<Employee> validateSalesPeriod(List<Employee> employees) {
        return employees.stream()
                .filter(employee -> employee.getSalesPeriod() <= report.getPeriodLimit())
                .collect(Collectors.toList());
    }

    private Map<Employee, Double> calculateEmployeeScores(List<Employee> filteredEmployees) {
        Map<Employee, Double> employeesScores = new HashMap<>();

        if (report.isUseExprienceMultiplier()) {
            filteredEmployees.forEach(employee -> employeesScores.put(employee,
                    (employee.getTotalSales() / employee.getSalesPeriod()) * employee.getExperienceMultiplier()));
        } else {
            filteredEmployees.forEach(employee -> employeesScores.put(employee,
                    (employee.getTotalSales() * 1.0 / employee.getSalesPeriod())));
        }

        return employeesScores;
    }

    private Map<Employee, Double> sortEmployeesByScore(Map<Employee, Double> employeesToSort) {
        List<Map.Entry<Employee, Double>> list = new LinkedList<>(employeesToSort.entrySet());
        list.sort(Map.Entry.comparingByValue(Comparator.reverseOrder()));

        Map<Employee, Double> sortedEmployeesByScore = new LinkedHashMap<>();
        for (Map.Entry<Employee, Double> mapKeyValue : list) {
            sortedEmployeesByScore.put(mapKeyValue.getKey(), mapKeyValue.getValue());
        }
        return sortedEmployeesByScore;
    }

    private Map<Employee, Double> findTopEmployees(Map<Employee, Double> evaluateEmployees) {
        int countOfTopEmployees = ((evaluateEmployees.size() * report.getTopPerformersThreshold()) / 100);
        if (countOfTopEmployees == 0) countOfTopEmployees = 1;
        Double minScoreFromTopEmployees = evaluateEmployees.values()
                .stream()
                .mapToDouble(Double::doubleValue)
                .toArray()[countOfTopEmployees - 1];

        Map<Employee, Double> topEmployees = new LinkedHashMap<>();
        evaluateEmployees.forEach((employee, score) -> {
            if (score >= minScoreFromTopEmployees) {
                topEmployees.put(employee, score);
            }
        });
        return topEmployees;
    }

}
