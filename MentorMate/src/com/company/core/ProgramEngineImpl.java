package com.company.core;

import com.company.core.contracts.EvaluationService;
import com.company.core.contracts.ProgramEngine;
import com.company.models.EmployeeImpl;
import com.company.models.ReportImpl;
import com.company.models.contracts.Employee;
import com.company.models.contracts.Report;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.exc.MismatchedInputException;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Files;
import java.nio.file.NoSuchFileException;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class ProgramEngineImpl implements ProgramEngine {

    private static final String ENGINE_EXCEPTION = "!!ENGINE EXCEPTION CAUGHT!! --> ";
    private static final String STEP_1 = "Step 1: Enter path to JSON file with Employees data:";
    private static final String STEP_2 = "Step 2: Enter path to JSON file with report definition data:";
    private static final String FILE_NOT_FOUND_EX = " (The system cannot find the file specified)";
    private static final String MISMATCHED_FILE_EXTENSION_EX = " (The file format MUST be formatted as the example!)";
    private final ObjectMapper objectMapper;
    private final Scanner scanner;

    public ProgramEngineImpl() {
        objectMapper = new ObjectMapper();
        scanner = new Scanner(System.in);
    }

    @Override
    public void start() {
        String exception = ENGINE_EXCEPTION;
        boolean done = true;
        while (done) {
            try {
                System.out.println(STEP_1);
                String path = scanner.nextLine();
                String jsonAsString = Files.readString(Paths.get(path));
                List<EmployeeImpl> listEmployee = objectMapper.readValue(jsonAsString, new TypeReference<>() {
                });
                List<Employee> employeesList = List.copyOf(listEmployee);

                System.out.println(STEP_2);
                String pathToReport = scanner.nextLine();
                Report report = objectMapper.readValue(new File(pathToReport), ReportImpl.class);
                EvaluationService evaluationService = new EvaluationServiceImpl(employeesList, report);
                evaluationService.createCSVFile();
                done = false;
            } catch (NoSuchFileException | FileNotFoundException ex) {
                System.out.println(exception + ex.getMessage() + FILE_NOT_FOUND_EX);
            } catch (MismatchedInputException ex) {
                System.out.println(exception + MISMATCHED_FILE_EXTENSION_EX);
            } catch (Exception ex) {
                System.out.println(exception + ex.getClass().getName() + ex.getMessage());

            }
        }
    }
}

