package com.company.core.contracts;


import com.company.models.contracts.Employee;
import com.company.models.contracts.Report;

import java.io.IOException;
import java.util.List;

public interface EvaluationService {
    List<Employee> getEmployees();

    Report getReport();

    void createCSVFile() throws IOException;
}
