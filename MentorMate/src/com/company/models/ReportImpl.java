package com.company.models;

import com.company.models.contracts.Report;

public class ReportImpl implements Report {
    private int topPerformersThreshold;
    private boolean useExprienceMultiplier;
    private int periodLimit;

    public ReportImpl() {
    }

    @Override
    public int getTopPerformersThreshold() {
        return topPerformersThreshold;
    }

    @Override
    public boolean isUseExprienceMultiplier() {
        return useExprienceMultiplier;
    }

    @Override
    public int getPeriodLimit() {
        return periodLimit;
    }

    private void setTopPerformersThreshold(int topPerformersThreshold) {
        if (this.topPerformersThreshold > 100) {
            throw new IllegalArgumentException(" Top performers threshold percents must be between 1% and 100%");
        }

        this.topPerformersThreshold = topPerformersThreshold;
    }

    private void setUseExprienceMultiplier(boolean useExprienceMultiplier) {
        this.useExprienceMultiplier = useExprienceMultiplier;
    }

    private void setPeriodLimit(int periodLimit) {
        this.periodLimit = periodLimit;
    }
}
