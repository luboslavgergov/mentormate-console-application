package com.company.models;

import com.company.models.contracts.Employee;

import java.util.Objects;

public class EmployeeImpl implements Employee {

    private String name;
    private int totalSales;
    private int salesPeriod;
    private double experienceMultiplier;

    public EmployeeImpl() {

    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getTotalSales() {
        return totalSales;
    }

    @Override
    public int getSalesPeriod() {
        return salesPeriod;
    }

    @Override
    public double getExperienceMultiplier() {
        return experienceMultiplier;
    }

    private void setName(String name) {
        this.name = name;
    }

    private void setTotalSales(int totalSales) {
        this.totalSales = totalSales;
    }

    private void setSalesPeriod(int salesPeriod) {
        this.salesPeriod = salesPeriod;
    }

    private void setExperienceMultiplier(double experienceMultiplier) {
        this.experienceMultiplier = experienceMultiplier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EmployeeImpl employee = (EmployeeImpl) o;
        return getTotalSales() == employee.getTotalSales() && getSalesPeriod() == employee.getSalesPeriod()
                && Double.compare(employee.getExperienceMultiplier(), getExperienceMultiplier()) == 0
                && Objects.equals(getName(), employee.getName());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getName(), getTotalSales(), getSalesPeriod(), getExperienceMultiplier());
    }
}
