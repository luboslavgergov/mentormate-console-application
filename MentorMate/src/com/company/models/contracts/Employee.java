package com.company.models.contracts;

public interface Employee {

    String getName();

    int getTotalSales();

    int getSalesPeriod();

    double getExperienceMultiplier();
}
