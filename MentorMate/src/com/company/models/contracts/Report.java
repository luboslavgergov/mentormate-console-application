package com.company.models.contracts;

public interface Report {
    int getTopPerformersThreshold();

    boolean isUseExprienceMultiplier();

    int getPeriodLimit();
}
