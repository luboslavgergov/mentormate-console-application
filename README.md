# MentorMate Console Application

Create a Java console application that generates monthly performance reports.

At the given input example of the files, there is a typo in the JSON report definition file. It is missed one "e" in useExprienceMultiplier. So I made the same mistake in the code in case you have ready input files to work with this typo. The format of the input files must be exactly the same as those shown in the pdf file.

![alt text](https://imgur.com/XogcDxX.png "Typo example")
